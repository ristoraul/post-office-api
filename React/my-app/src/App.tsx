import { Route } from 'react-router';
import { HomePage } from './components/pages/HomePage';


import './custom.css'
import Layout from './Layout';

// eslint-disable-next-line import/no-anonymous-default-export
export default () => (
    <Layout>
      <Route exact path='/'component={HomePage} />
    </Layout>
);
