import {parcelReducer} from "../components/reducers/ParcelReducer";
import * as Parcel from "../components/reducers/ParcelReducer";
import {shipmentReducer} from "../components/reducers/ShipmentReducer";
import * as Shipment from "../components/reducers/ShipmentReducer";
import {parcelsBagReducer} from "../components/reducers/ParcelsBagReducer";
import * as ParcelsBag from "../components/reducers/ParcelsBagReducer";
import {lettersBagReducer} from "../components/reducers/LettersBagReducer";
import * as LettersBag from "../components/reducers/LettersBagReducer";
export interface ApplicationState {
    parcel: Parcel.ParcelReducerType
    shipment: Shipment.ShipmentReducerType
    parcelsBag: ParcelsBag.ParcelReducerType
    lettersBag: LettersBag.LettersReducerType

    
}

// Whenever an action is dispatched, Redux will update each top-level application state property using
// the reducer with the matching name. It's important that the names match exactly, and that the reducer
// acts on the corresponding ApplicationState property type.
export const reducers = {
    parcel: parcelReducer,
    shipment: shipmentReducer,
    parcelsBag: parcelsBagReducer,
    lettersBag: lettersBagReducer
    

};

// This type can be used as a hint on action creators so that its 'dispatch' and 'getState' params are
// correctly typed to match your store.
export interface AppThunkAction<TAction> {
    (dispatch: (action: TAction) => void, getState: () => ApplicationState): void;
}
