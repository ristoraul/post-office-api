export type ParcelsBag = {
    id?: number,
    bagNr:string,
    shipmentId:number,
    amountOfParcels?: number,
    totalPrice?: number,
    bagType?: string
}