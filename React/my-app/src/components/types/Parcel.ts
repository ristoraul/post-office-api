export type Parcel = {
    id?: number,
    bagId: number
    parcelNr: string,
    recipientName: string,
    destinationCountry: string,
    weight: number,
    price: number
}