import { LettersBag } from "./LettersBag";
import { ParcelsBag } from "./ParcelsBag";

export type Shipment = {
    id?: number,
    shipmentNr : string,
    airport:string,
    flightNr: string,
    flightDate: Date,
    isShipmentFinalized?: boolean
    bagsOfParcels?: ParcelsBag[]
    bagsOfLetters?: LettersBag[]
}