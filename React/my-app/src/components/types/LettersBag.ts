export type LettersBag = {
    id?: number,
    bagNr:string,
    shipmentId:number,
    countOfLetters:number,
    weight:number,
    price:number
    bagType?: string
}