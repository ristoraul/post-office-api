import React, { useEffect, useRef, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Button, Collapse } from "reactstrap";
import { ApplicationState } from "../../store";
import Loading from "../components/Loading";
import ParcelsBagForm from "../components/ParcelsBagForm";
import ShipmentForm from "../components/ShipmentForm";
import ShipmentTable from "../components/ShipmentTable";
import { getAllShipments } from "../queries/ShipmentQueries";
import { Shipment } from "../types/Shipment";
import "../componentStyles/homePage.css";
import ShipmentDetails from "../components/ShipmentDetails";
import _ from "lodash";
import LettersBagForm from "../components/LettersBagForm";
import ParcelForm from "../components/ParcelForm";

export const HomePage = () => {
  const dispatch = useDispatch();
  const renders = useRef(0);
  console.log("Home page renders: ", renders.current++)

  const shipments = useSelector<ApplicationState, Shipment[]>(
    (state) => state.shipment.shipments
  );
  const shipmentsFetching = useSelector<ApplicationState, boolean>(
    (state) => state.shipment.shipmentsFetching
  );
  const selectedShipment = useSelector<ApplicationState, Shipment>(
    (state) => state.shipment.selectedShipment
  );
  const errors = useSelector<ApplicationState, string>(
    (state) => state.shipment.error
  );
  const putErrors = useSelector<ApplicationState, any>(
    (state) => state.shipment.putError
  );
  const [addCourseFormOpen, setAddCourseFormOpen] = useState<boolean>(false);
  const [parcelsBagFormOpen, setParcelsBagFormOpen] = useState<boolean>(false);
  const [lettersBagFormOpen, setLettersBagFormOpen] = useState<boolean>(false);
  const [parcelFormOpen, setParcelFormOpen] = useState<boolean>(false);
  const handleToggleCourseFormOpen = () => {
    setAddCourseFormOpen((state) => !state);
  };
  const handleToggleParcelsBagFormOpen = () => {
    setParcelsBagFormOpen((state) => !state);
  };
  const handleToggleLettersBagFormOpen = () => {
    setLettersBagFormOpen((state) => !state);
  };

  useEffect(() => {
    getAllShipments(dispatch);

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <React.Fragment>
      <div>
        <div className="col-md-6">
          <Button
            onClick={handleToggleCourseFormOpen}
            style={{ margin: "15px 0px" }}
            color={addCourseFormOpen ? "danger" : "primary"}
          >
            {addCourseFormOpen ? "Close" : "Add shipment"}
          </Button>
          <Collapse isOpen={addCourseFormOpen}>
            <h4>Add shipment</h4>
            <ShipmentForm />
          </Collapse>
        </div>
        <div className="col-md-6">
          <Button
            onClick={handleToggleParcelsBagFormOpen}
            style={{ margin: "15px 0px" }}
            color={parcelsBagFormOpen ? "danger" : "primary"}
          >
            {parcelsBagFormOpen ? "Close" : "Add bag with parcels"}
          </Button>
          <Collapse isOpen={parcelsBagFormOpen}>
            <h4>Add bag with parcels</h4>
            <ParcelsBagForm />
          </Collapse>
        </div>
        <div className="col-md-6">
          <Button
            onClick={handleToggleLettersBagFormOpen}
            style={{ margin: "15px 0px" }}
            color={lettersBagFormOpen ? "danger" : "primary"}
          >
            {lettersBagFormOpen ? "Close" : "Add bag with letters"}
          </Button>
          <Collapse isOpen={lettersBagFormOpen}>
            <h4>Add bag with letters</h4>
            <LettersBagForm />
          </Collapse>
        </div>
        <div className="col-md-6">
          <Button
            onClick={() => setParcelFormOpen(!parcelFormOpen)}
            style={{ margin: "15px 0px" }}
            color={parcelFormOpen ? "danger" : "primary"}
          >
            {parcelFormOpen ? "Close" : "Add Parcel"}
          </Button>
          <Collapse isOpen={parcelFormOpen}>
            <h4>Add Parcel</h4>
            <ParcelForm />
          </Collapse>
        </div>
      </div>
      <p style={{ color: "red" }}>{errors}</p>
      {!_.isEmpty(putErrors.errors) ? (
        <div className="error">
          {Object.keys(putErrors.errors).map((x) => {
            return <p style={{ color: "red" }}>{putErrors.errors[x]}</p>;
          })}
        </div>
      ) : (
        ""
      )}

      {!shipmentsFetching ? <ShipmentTable data={shipments} /> : <Loading />}
      {!_.isEmpty(selectedShipment) ? (
        <ShipmentDetails data={selectedShipment} />
      ) : (
        ""
      )}
    </React.Fragment>
  );
};
