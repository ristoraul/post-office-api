type Action = {
  type: any;
  payload: any;
};

export type LettersReducerType = {
    postError:any
};

const initialState: LettersReducerType = {
    postError: {}
};
export const lettersBagReducer = (state = initialState, action: Action) => {
    switch (action.type) {
        case "POST_LETTERSBAG_ERROR": {
          return {
            ...state,
            postError: action.payload
          };
        }
        case "POST_LETTERSBAG_SUCCESS": {
          return {
            ...state,
            postError: action.payload
          };
        }

        default:
          return state;
      }
    }