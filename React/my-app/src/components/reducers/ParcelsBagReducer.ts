import { ParcelsBag } from "../types/ParcelsBag";

type Action = {
  type: any;
  payload: any;
};

export type ParcelReducerType = {
    parcelsBags: ParcelsBag[];
    parcelsBagFetching: boolean;
    error: any;
    postError?:any
};

const initialState: ParcelReducerType = {
    parcelsBags: [],
    parcelsBagFetching: false,
    error: "",
    postError: {}
};
export const parcelsBagReducer = (state = initialState, action: Action) => {
    switch (action.type) {
      case "FETCH_PARCELSBAGS_START": {
        return {
          ...state,
          parcelsBagFetching: true
        };
      }
      case "FETCH_PARCELSBAGS_SUCCESS": {
        return {
          ...state,
          parcelsBags: action.payload,
          parcelsBagFetching: false
        };
      }
      case "FETCH_PARCELSBAGS_ERROR": {
        return {
          ...state,
          parcelsBagFetching: false,
          error: action.payload
        };
      }
        case "POST_PARCELSBAG_ERROR": {
          return {
            ...state,
            postError: action.payload
          };
        }
        case "POST_PARCELSBAG_SUCCESS": {
          return {
            ...state,
            postError: action.payload
          };
        }

        default:
          return state;
      }
    }