import { Shipment } from "../types/Shipment";

type Action = {
  type: any;
  payload: any;
};

export type ShipmentReducerType = {
    shipments: Shipment[];
    shipmentsFetching: boolean;
    selectedShipment: Shipment;
    error: any;
    postError:any;
    putError: any;
};

const initialState: ShipmentReducerType = {
    shipments: [],
    shipmentsFetching: false,   
    selectedShipment: {} as Shipment,
    error: "",
    postError:{},
    putError: {}

};

export const shipmentReducer = (state = initialState, action: Action) => {
    switch (action.type) {
        case "FETCH_SHIPMENTS_START": {
          return {
            ...state,
            shipmentsFetching: true
          };
        }
        case "FETCH_SHIPMENTS_SUCCESS": {
          return {
            ...state,
            shipments: action.payload,
            shipmentsFetching: false
          };
        }
        case "FETCH_SHIPMENTS_ERROR": {
          return {
            ...state,
            shipmentsFetching: false,
            error: action.payload
          };
        }
        case "POST_SHIPMENT_ERROR": {
          return {
            ...state,
            postError: action.payload
          };
        }
        case "POST_SHIPMENT_SUCCESS": {
          return {
            ...state,
            postError: action.payload
          };
        }
        case "SELECT_SHIPMENT": {
          return {
            ...state,
            selectedShipment: action.payload
          };
        }
        case "PUT_SHIPMENT_ERROR": {
          return {
            ...state,
            putError: action.payload
          };
        }
        case "PUT_SHIPMENT_SUCCESS": {
          return {
            ...state,
            putError: action.payload
          };
        }  

        default:
          return state;
      }
}