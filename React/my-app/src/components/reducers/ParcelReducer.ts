import { Parcel } from "../types/Parcel";

type Action = {
  type: any;
  payload: any;
};

export type ParcelReducerType = {
    parcels: Parcel[];
    parcelFetching: boolean;
    error: any;
    postError: any
};

const initialState: ParcelReducerType = {
    parcels: [],
    parcelFetching: false,
    error: "",
    postError: {}
};

export const parcelReducer = (state = initialState, action: Action) => {
    switch (action.type) {
        case "FETCH_PARCELS_START": {
          return {
            ...state,
            parcelFetching: true
          };
        }
        case "FETCH_PARCELS_SUCCESS": {
          return {
            ...state,
            parcels: action.payload,
            parcelFetching: false
          };
        }
        case "FETCH_PARCELS_ERROR": {
          return {
            ...state,
            parcelFetching: false,
            error: action.payload
          };
        }
        case "POST_PARCEL_ERROR": {
          return {
            ...state,
            postError: action.payload
          };
        }
        case "POST_PARCEL_SUCCESS": {
          return {
            ...state,
            postError: action.payload
          };
        }
        default:
          return state;
      }
}