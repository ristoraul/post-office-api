import { ParcelsBag } from "../types/ParcelsBag";

export const POST_PARCELSBAG_ERROR= (payload: any) => ({
    type: "POST_PARCELSBAG_ERROR",
    payload: payload
  });
  export const POST_PARCELSBAG_SUCCESS= (payload: any) => ({
    type: "POST_PARCELSBAG_SUCCESS",
    payload: payload
  });
  export const FETCH_PARCELSBAGS_START = () => ({
    type: "FETCH_PARCELSBAGS_START"
  });
  export const FETCH_PARCELSBAGS_SUCCESS = (payload: ParcelsBag) => ({
    type: "FETCH_PARCELSBAGS_SUCCESS",
    payload: payload
  });
  export const FETCH_PARCELSBAGS_ERROR = (payload: any) => ({
    type: "FETCH_PARCELSBAGS_ERROR",
    payload: payload
  });