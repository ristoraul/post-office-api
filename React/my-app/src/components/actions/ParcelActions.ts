import { Parcel } from "../types/Parcel";

export const FETCH_PARCELS_START = () => ({
    type: "FETCH_PARCELS_START"
  });
  export const FETCH_PARCELS_SUCCESS = (payload: Parcel) => ({
    type: "FETCH_PARCELS_SUCCESS",
    payload: payload
  });
  export const FETCH_PARCELS_ERROR = (payload: any) => ({
    type: "FETCH_PARCELS_ERROR",
    payload: payload
  });
  export const POST_PARCEL_ERROR= (payload: any) => ({
    type: "POST_PARCEL_ERROR",
    payload: payload
  });
  export const POST_PARCEL_SUCCESS= (payload: any) => ({
    type: "POST_PARCEL_SUCCESS",
    payload: payload
  });