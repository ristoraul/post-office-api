import { Shipment } from "../types/Shipment";

export const FETCH_SHIPMENTS_START = () => ({
    type: "FETCH_SHIPMENTS_START"
  });
  export const FETCH_SHIPMENTS_SUCCESS = (payload: Shipment) => ({
    type: "FETCH_SHIPMENTS_SUCCESS",
    payload: payload
  });
  export const FETCH_SHIPMENTS_ERROR = (payload: any) => ({
    type: "FETCH_SHIPMENTS_ERROR",
    payload: payload
  });
  export const POST_SHIPMENT_ERROR= (payload: any) => ({
    type: "POST_SHIPMENT_ERROR",
    payload: payload
  });
  export const POST_SHIPMENT_SUCCESS= (payload: any) => ({
    type: "POST_SHIPMENT_SUCCESS",
    payload: payload
  });
  export const SELECT_SHIPMENT = (payload:any) => ({
    type: "SELECT_SHIPMENT",
    payload: payload
  })
  export const PUT_SHIPMENT_ERROR= (payload: any) => ({
    type: "PUT_SHIPMENT_ERROR",
    payload: payload
  });
  export const PUT_SHIPMENT_SUCCESS= (payload: any) => ({
    type: "PUT_SHIPMENT_SUCCESS",
    payload: payload
  });