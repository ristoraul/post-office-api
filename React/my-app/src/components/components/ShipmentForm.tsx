import React, { useRef, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Button, Form, FormGroup, Input, Label } from "reactstrap";
import { ApplicationState } from "../../store";
import "../componentStyles/form.css";
import { addNewShipment, getAllShipments } from "../queries/ShipmentQueries";
import _ from "lodash";
var DatePicker = require("reactstrap-date-picker");

const ShipmentForm = () => {
  const dispatch = useDispatch();
  const renders = useRef(0);
  console.log("ShipmentForm renders: ", renders.current++)
  const postErrors = useSelector<ApplicationState, any>(
    (state) => state.shipment.postError
  );
  const fetchingErrors = useSelector<ApplicationState, string>(
    (state) => state.shipment.error
  );
  const [shipmentNr, setShipmentNr] = useState<string>("");
  const [airport, setAirport] = useState<string>("TLL");
  const [flightNr, setFlightNr] = useState<string>("");
  const [flightDate, setFlightDate] = useState(new Date().toISOString());
  const [clientErrors, setClientErrors] = useState<string>("");

  const handleAirportChange: (e: React.ChangeEvent<HTMLInputElement>) => void =
    (e) => {
      setAirport(e.target.value);
    };
  const handleShipmentNr: (e: React.ChangeEvent<HTMLInputElement>) => void = (
    e
  ) => {
    setShipmentNr(e.target.value);
  };
  const handleFlightNr: (e: React.ChangeEvent<HTMLInputElement>) => void = (
    e
  ) => {
    setFlightNr(e.target.value);
  };
  const handleAddShipment = () => {
    if (
      shipmentNr !== "" &&
      airport !== "" &&
      flightDate !== "" &&
      flightNr !== ""
    ) {
      addNewShipment(
        {
          shipmentNr: shipmentNr,
          airport: airport,
          flightNr: flightNr,
          flightDate: new Date(flightDate),
        },
        dispatch
      ).then(() => {
        getAllShipments(dispatch);
        setClientErrors("");
      });
    }
    setClientErrors("Insufficient data");
  };
  return (
    <Form>
      <FormGroup>
        <Label>Shipment number</Label>
        <Input
          type="text"
          value={shipmentNr}
          onChange={handleShipmentNr}
          placeholder="XXX-XXXXXX"
        />
      </FormGroup>
      <FormGroup>
        <Label>Airport</Label>
        <Input type="select" value={airport} onChange={handleAirportChange}>
          <option value="TLL">TLL</option>
          <option value="RIX">RIX</option>
          <option value="HEL">HEL</option>
        </Input>
      </FormGroup>
      <FormGroup>
        <Label>Flight number</Label>
        <Input
          type="text"
          value={flightNr}
          onChange={handleFlightNr}
          placeholder="LL1234"
        />
      </FormGroup>
      <FormGroup>
        <Label>Flight date</Label>
        <DatePicker
          className="modal-date-form-control"
          value={flightDate}
          onChange={(date: any) => setFlightDate(date)}
          dateFormat="DD/MM/YYYY"
          showClearButton={false}
          clearButtonElement={""}
        />
      </FormGroup>
      <FormGroup>
        <Button
          color="success"
          onClick={handleAddShipment}
          disabled={!_.isEmpty(fetchingErrors)}
        >
          ADD
        </Button>
      </FormGroup>
      {!_.isEmpty(postErrors.errors) ? (
        <div className="error">
          {Object.keys(postErrors.errors).map((x) => {
            return <p style={{ color: "red" }}>{postErrors.errors[x]}</p>;
          })}
        </div>
      ) : (
        ""
      )}
      <p style={{ color: "red" }}>{clientErrors}</p>
    </Form>
  );
};
export default ShipmentForm;
