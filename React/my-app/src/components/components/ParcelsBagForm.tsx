import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Button, Form, FormGroup, Input, Label } from "reactstrap";
import { ApplicationState } from "../../store";
import "../componentStyles/form.css";
import { getAllShipments } from "../queries/ShipmentQueries";
import _ from "lodash";
import { Shipment } from "../types/Shipment";
import { addNewParcelsBag, getAllParcelsBags } from "../queries/ParcelsBagQueries";

const ParcelsBagForm = () => {
  const dispatch = useDispatch();
  const postErrors = useSelector<ApplicationState, any>(
    (state) => state.parcelsBag.postError
  );
  const shipments = useSelector<ApplicationState, Shipment[]>(
    (state) => state.shipment.shipments
  );
  const fetchingErrors = useSelector<ApplicationState, string>(
    (state) => state.shipment.error
  );
  const [bagNr, setBagNr] = useState<string>("");
  const [shipment, setShipment] = useState<string>("");
  const [clientErrors, setClientErrors] = useState<string>("");

  const handleBagNr: (e: React.ChangeEvent<HTMLInputElement>) => void = (e) => {
    setBagNr(e.target.value);
  };
  const handleShipment: (e: React.ChangeEvent<HTMLInputElement>) => void = (
    e
  ) => {
    setShipment(e.target.value);
  };
  const handleAddNewParcelsBag = () => {
    if (bagNr !== "" && shipment !== "") {
      addNewParcelsBag(
        { bagNr: bagNr, shipmentId: parseInt(shipment) },
        dispatch
      ).then(() => {
        getAllShipments(dispatch);
        getAllParcelsBags(dispatch);
        setClientErrors("");
      });
    }
    setClientErrors("Insufficient data");
  };

  useEffect(() => {
    getAllShipments(dispatch);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <Form>
      <FormGroup>
        <Label>Bag number</Label>
        <Input type="text" value={bagNr} onChange={handleBagNr} />
      </FormGroup>
      <FormGroup>
        <Label>Shipment</Label>
        <Input type="select" value={shipment} onChange={handleShipment}>
          <option value=""></option>
          {shipments?.map((shipment) => {
            return <option value={shipment.id}>{shipment.shipmentNr}</option>;
          })}
        </Input>
      </FormGroup>

      <FormGroup>
        <Button
          color="success"
          onClick={handleAddNewParcelsBag}
          disabled={!_.isEmpty(fetchingErrors)}
        >
          ADD
        </Button>
      </FormGroup>
      {!_.isEmpty(postErrors.errors) ? (
        <div className="error">
          {Object.keys(postErrors.errors).map((x) => {
            return <p style={{ color: "red" }}>{postErrors.errors[x]}</p>;
          })}
        </div>
      ) : (
        ""
      )}
      <p style={{ color: "red" }}>{clientErrors}</p>
    </Form>
  );
};
export default ParcelsBagForm;
