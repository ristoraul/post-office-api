import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Button, Form, FormGroup, Input, Label } from "reactstrap";
import { ApplicationState } from "../../store";
import "../componentStyles/form.css";
import { getAllShipments } from "../queries/ShipmentQueries";
import _ from "lodash";
import { Shipment } from "../types/Shipment";
import { addNewLettersBag } from "../queries/LettersBagQueries";

const LettersBagForm = () => {
  const dispatch = useDispatch();
  const postErrors = useSelector<ApplicationState, any>(
    (state) => state.lettersBag.postError
  );
  const shipments = useSelector<ApplicationState, Shipment[]>(
    (state) => state.shipment.shipments
  );
  const fetchingErrors = useSelector<ApplicationState, string>(
    (state) => state.shipment.error
  );
  const [bagNr, setBagNr] = useState<string>("");
  const [countOfLetters, setCountOfLetters] = useState<number>(0);
  const [weight, setWeight] = useState<number>(0);
  const [price, setPrice] = useState<number>(0);
  const [shipment, setShipment] = useState<string>("");
  const [clientErrors, setClientErrors] = useState<string>("");
  const handleBagNr: (e: React.ChangeEvent<HTMLInputElement>) => void = (e) => {
    setBagNr(e.target.value);
  };
  const handleCountOfLetters: (e: React.ChangeEvent<HTMLInputElement>) => void =
    (e) => {
      setCountOfLetters(parseInt(e.target.value));
    };
  const handleWeight: (e: React.ChangeEvent<HTMLInputElement>) => void = (
    e
  ) => {
    setWeight(parseFloat(e.target.value));
  };
  const handlePrice: (e: React.ChangeEvent<HTMLInputElement>) => void = (e) => {
    setPrice(parseFloat(e.target.value));
  };
  const handleShipment: (e: React.ChangeEvent<HTMLInputElement>) => void = (
    e
  ) => {
    setShipment(e.target.value);
  };
  const handleAddNewLettersBag = () => {
    if (
      !isNaN(weight) &&
      !isNaN(price) &&
      !isNaN(countOfLetters) &&
      shipment !== ""
    ) {
      addNewLettersBag(
        {
          bagNr: bagNr,
          shipmentId: parseInt(shipment),
          countOfLetters: countOfLetters,
          weight: weight,
          price: price,
        },
        dispatch
      ).then(() => {
        getAllShipments(dispatch);
        setClientErrors("");
      });
    }
    setClientErrors("Insufficient data");
  };

  useEffect(() => {
    getAllShipments(dispatch);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <Form>
      <FormGroup>
        <Label>Bag number</Label>
        <Input type="text" value={bagNr} onChange={handleBagNr} />
      </FormGroup>
      <FormGroup>
        <Label>Count of letters</Label>
        <Input
          type="number"
          value={countOfLetters}
          onChange={handleCountOfLetters}
        />
      </FormGroup>
      <FormGroup>
        <Label>Weight</Label>
        <Input type="number" value={weight} onChange={handleWeight} />
      </FormGroup>
      <FormGroup>
        <Label>Price</Label>
        <Input type="number" value={price} onChange={handlePrice} />
      </FormGroup>
      <FormGroup>
        <Label>Shipment</Label>
        <Input type="select" value={shipment} onChange={handleShipment}>
          <option value=""></option>
          {shipments?.map((shipment) => {
            return <option value={shipment.id}>{shipment.shipmentNr}</option>;
          })}
        </Input>
      </FormGroup>

      <FormGroup>
        <Button
          color="success"
          onClick={handleAddNewLettersBag}
          disabled={!_.isEmpty(fetchingErrors)}
        >
          ADD
        </Button>
      </FormGroup>
      {!_.isEmpty(postErrors.errors) ? (
        <div className="error">
          {Object.keys(postErrors.errors).map((x) => {
            return (
              <div>
                <p style={{ color: "red" }}>{postErrors.errors[x]}</p>
              </div>
            );
          })}
        </div>
      ) : (
        ""
      )}
      <p style={{ color: "red" }}>{clientErrors}</p>
    </Form>
  );
};
export default LettersBagForm;
