import React, { useEffect, useRef, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Button, Form, FormGroup, Input, Label } from "reactstrap";
import { ApplicationState } from "../../store";
import "../componentStyles/form.css";
import { getAllShipments } from "../queries/ShipmentQueries";
import _ from "lodash";
import { ParcelsBag } from "../types/ParcelsBag";
import { addNewParcel } from "../queries/ParcelQueries";
import { getAllParcelsBags } from "../queries/ParcelsBagQueries";

const ParcelForm = () => {
  const dispatch = useDispatch();
  const renders = useRef(0);
  console.log("ParcelForm renders: ", renders.current++)
  const postErrors = useSelector<ApplicationState, any>(
    (state) => state.parcel.postError
  );
  const parcelsBags = useSelector<ApplicationState, ParcelsBag[]>(
    (state) => state.parcelsBag.parcelsBags
  );
  const fetchingErrors = useSelector<ApplicationState, string>(
    (state) => state.shipment.error
  );
  const [parcelNr, setParcelNr] = useState<string>("");
  const [recipientName, setRecipientName] = useState<string>("");
  const [destinationCountry, setDestionationCountry] = useState<string>("");
  const [weight, setWeight] = useState<number>(0);
  const [price, setPrice] = useState<number>(0);
  const [parcelsBag, setParcelsBag] = useState<string>("");
  const [clientErrors, setClientErrors] = useState<string>("");

  const handleAddNewParcel = () => {
    if (
      parcelNr !== "" &&
      recipientName !== "" &&
      destinationCountry !== "" &&
      !isNaN(weight) &&
      !isNaN(price) &&
      parcelsBag !== ""
    ) {
      addNewParcel(
        {
          parcelNr: parcelNr,
          recipientName: recipientName,
          price: price,
          weight: weight,
          bagId: parseInt(parcelsBag),
          destinationCountry: destinationCountry,
        },
        dispatch
      ).then(() => {
        getAllShipments(dispatch);
        setClientErrors("");
      });
    }
    setClientErrors("Insufficient data");
  };

  useEffect(() => {
    getAllParcelsBags(dispatch);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <Form>
      <FormGroup>
        <Label>Parcel number</Label>
        <Input
          type="text"
          value={parcelNr}
          onChange={(e: React.ChangeEvent<HTMLInputElement>) =>
            setParcelNr(e.target.value)
          }
          placeholder="LL123456LL"
        />
      </FormGroup>
      <FormGroup>
        <Label>Recipient name</Label>
        <Input
          type="text"
          value={recipientName}
          onChange={(e: React.ChangeEvent<HTMLInputElement>) =>
            setRecipientName(e.target.value)
          }
        />
      </FormGroup>
      <FormGroup>
        <Label>Destination country</Label>
        <Input
          type="text"
          value={destinationCountry}
          onChange={(e: React.ChangeEvent<HTMLInputElement>) =>
            setDestionationCountry(e.target.value)
          }
          placeholder="ET"
        />
      </FormGroup>
      <FormGroup>
        <Label>Weight</Label>
        <Input
          type="number"
          value={weight}
          onChange={(e: React.ChangeEvent<HTMLInputElement>) =>
            setWeight(parseFloat(e.target.value))
          }
        />
      </FormGroup>
      <FormGroup>
        <Label>Price</Label>
        <Input
          type="number"
          value={price}
          onChange={(e: React.ChangeEvent<HTMLInputElement>) =>
            setPrice(parseFloat(e.target.value))
          }
        />
      </FormGroup>
      <FormGroup>
        <Label>Bag with parcels</Label>
        <Input type="select" value={parcelsBag} onChange={(e: React.ChangeEvent<HTMLInputElement>) => setParcelsBag(e.target.value)}>
        <option value=""></option>
          {parcelsBags?.map((parcelsBag) => {
            return <option value={parcelsBag.id}>{parcelsBag.bagNr}</option>;
          })}
         
        </Input>
      </FormGroup>
      <FormGroup>
        <Button
          color="success"
          onClick={handleAddNewParcel}
          disabled={!_.isEmpty(fetchingErrors)}
        >
          ADD
        </Button>
      </FormGroup>
      {!_.isEmpty(postErrors.errors) ? (
        <div className="error">
          {Object.keys(postErrors.errors).map((x) => {
            return <p style={{ color: "red" }}>{postErrors.errors[x]}</p>;
          })}
        </div>
      ) : (
        ""
      )}
      <p style={{ color: "red" }}>{clientErrors}</p>
    </Form>
  );
};
export default ParcelForm;
