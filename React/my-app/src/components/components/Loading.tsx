import "../componentStyles/loading.css"

const Loading = () => {

    return (
        <div>
            <h4 className="loading-text-container">Loading</h4>
            <div className="loading-spinner-container">
            <div className="spinner-border" role="status">
            <span className="sr-only"></span>
            </div>  
            </div>
        </div>  
    )
};
export default Loading;
