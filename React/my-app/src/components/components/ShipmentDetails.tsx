import { Card, CardBody, CardSubtitle, CardText, CardTitle } from "reactstrap";
import { Shipment } from "../types/Shipment";
import "../componentStyles/shipmentDetails.css"

const ShipmentDetails: React.FC<{ data: Shipment }> = ({ data }) => {
  return (
    <div className="card">
      <Card>
        <CardBody>
          <CardTitle style={{ fontSize: "20px", fontWeight: "bold" }}>
            {data.shipmentNr}
          </CardTitle>
          <p>
            <span style={{ fontWeight: "bold" }}>Flight number: </span>
            {data.flightNr}
          </p>
          <p>
            <span style={{ fontWeight: "bold" }}>Flight date: </span>
            {data.flightDate}
          </p>
          <CardSubtitle style={{ fontWeight: "bold" }}>Bags:</CardSubtitle>
          <div className="row">
            <div className="col-md-5">
              <CardSubtitle style={{ fontWeight: "bold" }}>
                Bags with parcels:
              </CardSubtitle>
              <CardText>
                {data.bagsOfParcels?.length ? (
                  data.bagsOfParcels.map((x) => {
                    return (
                      <ul>
                        <li>
                          <p>
                            <span style={{ fontWeight: "bold" }}>
                              Bag number:{" "}
                            </span>
                            {x.bagNr}
                          </p>
                          <CardText>Bag type: {x.bagType}</CardText>
                          <CardText>
                            Parcels inside: {x.amountOfParcels}
                          </CardText>
                          <CardText>Total price: {x.totalPrice}</CardText>
                        </li>
                      </ul>
                    );
                  })
                ) : (
                  <p>Shipment doesn't include bags</p>
                )}
              </CardText>
            </div>
            <div className="col-md-5 col-md-offset-2">
              <CardSubtitle style={{ fontWeight: "bold" }}>
                Bags with letters:
              </CardSubtitle>
              <CardText>
                {data.bagsOfLetters?.length ? (
                  data.bagsOfLetters.map((x) => {
                    return (
                      <ul>
                        <li>
                          <p>
                            <span style={{ fontWeight: "bold" }}>
                              Bag number:{" "}
                            </span>
                            {x.bagNr}
                          </p>
                          <CardText>Bag type: {x.bagType}</CardText>
                          <CardText>
                            Letters inside: {x.countOfLetters}
                          </CardText>
                          <CardText>Price: {x.price}</CardText>
                        </li>
                      </ul>
                    );
                  })
                ) : (
                  <p>Shipment doesn't include bags</p>
                )}
              </CardText>
            </div>
          </div>
        </CardBody>
      </Card>
    </div>
  );
};

export default ShipmentDetails;
