import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { Button, Table } from "reactstrap";
import { ApplicationState } from "../../store";
import { SELECT_SHIPMENT } from "../actions/ShipmentActions";
import "../componentStyles/shipmentTable.css";
import { deleteShipmentById, getAllShipments, updateShipment } from "../queries/ShipmentQueries";
import { Shipment } from "../types/Shipment";
import AlertDialog from "./AlertDialog";

type TableProps = {
  data: Shipment[];
};

const ShipmentTable: React.FC<TableProps> = ({ data }) => {
  const dispatch = useDispatch();
  const selectedShipment = useSelector<ApplicationState, Shipment>(
    (state) => state.shipment.selectedShipment
  );
  const handleShipmentSelect = (shipment: Shipment) => {
    dispatch(SELECT_SHIPMENT(shipment));

  };
  
  const handleShipmentDelete = (shipmentid : any) => {
    deleteShipmentById(shipmentid).then(() => {
      getAllShipments(dispatch);
    })
  };
  const finalizeShipment = (shipment: Shipment) => {
    let finalizedShipment = {
      id: shipment.id,
      shipmentNr: shipment.shipmentNr,
      flightDate: shipment.flightDate,
      flightNr: shipment.flightNr,
      airport: shipment.airport,
      isShipmentFinalized: true
      
    } as Shipment;
    updateShipment(finalizedShipment, shipment.id!, dispatch).then(() => {
      getAllShipments(dispatch);
    });;
  };


  return (
    <Table className="test">
      <thead>
        <tr>
          <th>Shipment number</th>
          <th>Airport</th>
          <th>Flight number</th>
          <th>Flight date</th>
          <th>Is finalized?</th>
          <th></th>
        </tr>
      </thead>
      <tbody>
        {data?.map((shipment: Shipment, idx: any) => {
          return (
            <tr
              key={idx}
              className="table-tr"
              id={"Airport" + shipment.id}
              onClick={() => handleShipmentSelect(shipment)}
              style={{
                backgroundColor:
                  shipment.id === selectedShipment.id ? "#5bc0de" : "",
              }}
            >
              <td>{shipment.shipmentNr}</td>
              <td>{shipment.airport}</td>
              <td>{shipment.flightNr}</td>
              <td>{shipment.flightDate}</td>
              <td>{shipment.isShipmentFinalized!.toString()}</td>
              <td>
              <td><Button onClick={() => finalizeShipment(shipment)} className="finalizeBtn">
                Finalize
              </Button></td>
              <td><AlertDialog handleDelete={() => handleShipmentDelete(shipment.id!)}/></td>       
              </td>
            </tr>
          );
        })}
      </tbody>
    </Table>
  );
};
export default ShipmentTable;
