import axios from 'axios'
import {
    POST_PARCELSBAG_ERROR,
    POST_PARCELSBAG_SUCCESS,
    FETCH_PARCELSBAGS_ERROR,
    FETCH_PARCELSBAGS_START,
    FETCH_PARCELSBAGS_SUCCESS
} from "../actions/ParcelsBagActions"
import {apiUrl} from "../../config/apiconfig";
import { ParcelsBag } from '../types/ParcelsBag';

export const getAllParcelsBags: (dispatch: any) => void = (dispatch) => {
    dispatch(FETCH_PARCELSBAGS_START());
    fetchAllParcelsBags().then(data => dispatch(FETCH_PARCELSBAGS_SUCCESS(data.data))).catch(err => dispatch(FETCH_PARCELSBAGS_ERROR(err.message)));
}
export const fetchAllParcelsBags = () => {
    return axios.get(`${apiUrl}/api/parcelsbag`);
};

export const addNewParcelsBag = async (parcelsBag: ParcelsBag, dispatch: any) => {
   return await postNewParcelsBag(parcelsBag).then(dispatch(POST_PARCELSBAG_SUCCESS(""))).catch(err => dispatch(POST_PARCELSBAG_ERROR(err.response.data)));
 };
 
 const postNewParcelsBag = (parcelsBag: ParcelsBag) => {
     console.log(JSON.stringify(parcelsBag));
     return axios.post(`${apiUrl}/api/parcelsbag`, JSON.stringify(parcelsBag), {headers: {"Content-Type": "application/json"}});
 };