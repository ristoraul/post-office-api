import axios from 'axios'
import {
    FETCH_SHIPMENTS_ERROR,
    FETCH_SHIPMENTS_START,
    FETCH_SHIPMENTS_SUCCESS,
    POST_SHIPMENT_ERROR,
    POST_SHIPMENT_SUCCESS,
    PUT_SHIPMENT_ERROR,
    PUT_SHIPMENT_SUCCESS
} from "../actions/ShipmentActions"
import {apiUrl} from "../../config/apiconfig";
import { Shipment } from '../types/Shipment';

export const getAllShipments: (dispatch: any) => void = (dispatch) => {
    dispatch(FETCH_SHIPMENTS_START());
    fetchAllShipments().then(data => dispatch(FETCH_SHIPMENTS_SUCCESS(data.data))).catch(err => dispatch(FETCH_SHIPMENTS_ERROR(err.message)));
}
const fetchAllShipments = () => {
    return axios.get(`${apiUrl}/api/shipment`);
};
export const addNewShipment = async (shipment: Shipment, dispatch: any) => {
    await postNewShipment(shipment).then(dispatch(POST_SHIPMENT_SUCCESS(""))).catch(err => dispatch(POST_SHIPMENT_ERROR(err.response.data)));
 };
 
 const postNewShipment = (shipment: Shipment) => {
     console.log(JSON.stringify(shipment));
     return axios.post(`${apiUrl}/api/shipment`, JSON.stringify(shipment), {headers: {"Content-Type": "application/json"}});
 };
 export const updateShipment = async (shipment: Shipment, id: number, dispatch: any) => {
    await putShipment(shipment, id).then(dispatch(PUT_SHIPMENT_SUCCESS(""))).catch(err => dispatch(PUT_SHIPMENT_ERROR(err.response.data)));
 };
 
 const putShipment = (shipment: Shipment, id: number) => {
     console.log(JSON.stringify(shipment));
     return axios.put(`${apiUrl}/api/shipment/${id}`, JSON.stringify(shipment), {headers: {"Content-Type": "application/json"}});
 };
 export const deleteShipmentById = async (shipmentId: number) => {
    return await deleteShipment(shipmentId);
};
const deleteShipment = async (shipmentId: number) => {
    return axios.delete(`${apiUrl}/api/shipment/${shipmentId}`);
};

 
