import axios from 'axios'
import {
    FETCH_PARCELS_ERROR,
    FETCH_PARCELS_START,
    FETCH_PARCELS_SUCCESS,
    POST_PARCEL_ERROR,
    POST_PARCEL_SUCCESS
} from "../actions/ParcelActions"
import {apiUrl} from "../../config/apiconfig";
import { Parcel } from '../types/Parcel';

export const getAllParcels: (dispatch: any) => void = (dispatch) => {
    dispatch(FETCH_PARCELS_START());
    fetchAllParcels().then(data => dispatch(FETCH_PARCELS_SUCCESS(data.data))).catch(err => dispatch(FETCH_PARCELS_ERROR(err.message)));
}
export const fetchAllParcels = () => {
    return axios.get(`${apiUrl}/api/parcel`);
};
export const addNewParcel = async (parcel: Parcel, dispatch: any) => {
    return await postNewParcel(parcel).then(dispatch(POST_PARCEL_SUCCESS(""))).catch(err => dispatch(POST_PARCEL_ERROR(err.response.data)));
  };
  
  const postNewParcel = (parcel: Parcel) => {
      console.log(JSON.stringify(parcel));
      return axios.post(`${apiUrl}/api/parcel`, JSON.stringify(parcel), {headers: {"Content-Type": "application/json"}});
  };
