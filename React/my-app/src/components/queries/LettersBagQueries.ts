import axios from 'axios'
import {
    POST_LETTERSBAG_ERROR,
    POST_LETTERSBAG_SUCCESS
} from "../actions/LettersBagActions"
import {apiUrl} from "../../config/apiconfig";
import { LettersBag } from '../types/LettersBag';

export const addNewLettersBag = async (lettersBag: LettersBag, dispatch: any) => {
   return await postNewLettersBag(lettersBag).then(dispatch(POST_LETTERSBAG_SUCCESS(""))).catch(err => dispatch(POST_LETTERSBAG_ERROR(err.response.data)));
 };
 
 const postNewLettersBag = (lettersBag: LettersBag) => {
     console.log(JSON.stringify(lettersBag));
     return axios.post(`${apiUrl}/api/lettersbag`, JSON.stringify(lettersBag), {headers: {"Content-Type": "application/json"}});
 };