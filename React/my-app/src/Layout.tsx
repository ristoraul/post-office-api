import * as React from "react";
import { Container } from "reactstrap";

import "./custom.css"

// eslint-disable-next-line import/no-anonymous-default-export
export default (props: { children?: React.ReactNode }) => (
  <React.Fragment>
    <div className="bottom-right">Hi Helmes!</div>
    <div className="diamond"></div>
    
    <Container>{props.children}
    
    </Container>
    
    
    
  </React.Fragment>
);
