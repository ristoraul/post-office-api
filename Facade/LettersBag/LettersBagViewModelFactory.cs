﻿using Data;


namespace Facade.LettersBag
{
    public class LettersBagViewModelFactory
    {
        public static LettersBagViewModel Create(BagEntity o)
        {

            var v = new LettersBagViewModel()
            {
                Id = o.Id,
                BagNr = o.BagNr,
                ShipmentId = o.ShipmentId,
                Price = o.Price,
                Weight = o.Weight,
                CountOfLetters = o.CountOfLetters

            };

            return v;
        }
    }
}
