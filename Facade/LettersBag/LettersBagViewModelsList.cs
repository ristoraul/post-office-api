﻿using System.Collections.Generic;
using Data;

namespace Facade.LettersBag
{
    public class LettersBagViewModelsList : List<LettersBagViewModel>
    {
        public LettersBagViewModelsList(IEnumerable<BagEntity> l)
        {
            if (l is null) return;
            foreach (var e in l)
            {
                Add(LettersBagViewModelFactory.Create(e));
            }
        }
    }
}
