﻿using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;
using Data;

namespace Facade.LettersBag
{
    public class LettersBagViewModel : ViewModelBase
    {
        public string BagNr { get; set; }
        public int ShipmentId { get; set; }
        [JsonConverter(typeof(JsonStringEnumConverter))]
        public BagEntity.BagTypeEnum BagType { get; } = BagEntity.BagTypeEnum.Letters;
        public int CountOfLetters { get; set; }
        [RegularExpression(@"^\d+(\.\d{0,3})?$", ErrorMessage = "Weight can't have more than 3 decimal places")]
        public decimal Weight { get; set; }
        [RegularExpression(@"^\d+(\.\d{0,2})?$", ErrorMessage = "Price can't have more than 2 decimal places")]
        public decimal Price { get; set; }
    }
}
