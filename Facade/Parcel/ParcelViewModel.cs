﻿using System.ComponentModel.DataAnnotations;

namespace Facade.Parcel
{
    public class ParcelViewModel : ViewModelBase
    {

        public int BagId { get; set; }
        public string ParcelNr { get; set; }
        public string RecipientName { get; set; }
        public string DestinationCountry { get; set; }
        [RegularExpression(@"^\d+(\.\d{0,3})?$", ErrorMessage = "Weight can't have more than 3 decimal places")]
        public decimal Weight { get; set; }
        [RegularExpression(@"^\d+(\.\d{0,2})?$", ErrorMessage = "Price can't have more than 2 decimal places")]
        public decimal Price { get; set; }
    }
}
