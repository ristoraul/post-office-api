﻿using Data;

namespace Facade.Parcel
{
    public static class ParcelViewModelFactory
    {
        public static ParcelViewModel Create(ParcelEntity o)
        {
            return new ParcelViewModel
            {
                Id = o.Id,
                ParcelNr = o.ParcelNr,
                DestinationCountry = o.DestinationCountry,
                BagId = o.BagId,
                Price = o.Price,
                Weight = o.Weight,
                RecipientName = o.RecipientName
            };
        }
    }
}
