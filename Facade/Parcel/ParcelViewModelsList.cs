﻿using System.Collections.Generic;
using Data;

namespace Facade.Parcel
{
    public class ParcelViewModelsList : List<ParcelViewModel>
    {
        public ParcelViewModelsList(IEnumerable<ParcelEntity> l)
        {
            if (l is null) return;
            foreach (var e in l)
            {
                Add(ParcelViewModelFactory.Create(e));
            }
        }
    }
}
