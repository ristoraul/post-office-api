﻿using System.Collections.Generic;
using Data;

namespace Facade.ParcelsBag
{
    public class ParcelsBagViewModelsList : List<ParcelsBagViewModel>
    {
        public ParcelsBagViewModelsList(IEnumerable<BagEntity> l)
        {
            if (l is null) return;
            foreach (var e in l)
            {
                Add(ParcelsBagViewModelFactory.Create(e));
            }
        }
    }
}
