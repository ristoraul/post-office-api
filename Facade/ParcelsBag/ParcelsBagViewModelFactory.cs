﻿using Data;

namespace Facade.ParcelsBag
{
    public class ParcelsBagViewModelFactory
    {
        public static ParcelsBagViewModel Create(BagEntity o)
        {

            var v = new ParcelsBagViewModel
            {
                Id = o.Id,
                BagNr = o.BagNr,
                //BagType = o.BagType,
                ShipmentId = o.ShipmentId
            };
            foreach (var c in o.Parcels)
            {
                //var parcel = ParcelViewModelFactory.Create(c);
                //v.Parcels.Add(parcel);
                v.TotalPrice += c.Price;
                v.AmountOfParcels = o.Parcels.Count;
            }

            return v;
        }
    }
}
