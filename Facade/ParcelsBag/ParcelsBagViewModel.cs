﻿using System.Text.Json.Serialization;
using Data;

namespace Facade.ParcelsBag
{
    public class ParcelsBagViewModel : ViewModelBase
    {
        public string BagNr { get; set; }

        [JsonConverter(typeof(JsonStringEnumConverter))]
        public BagEntity.BagTypeEnum BagType { get; } = BagEntity.BagTypeEnum.Parcels;
        public int ShipmentId { get; set; }
        public int AmountOfParcels { get; set; }
        public decimal TotalPrice { get; set; }


    }
}
