﻿using System;
using System.Collections.Generic;
using System.Text.Json.Serialization;
using Data;
using Facade.LettersBag;
using Facade.ParcelsBag;

namespace Facade.Shipment
{
    public class ShipmentViewModel : ViewModelBase
    {
        public string ShipmentNr { get; set; }
        [JsonConverter(typeof(JsonStringEnumConverter))]
        public Airport Airport { get; set; }
        public string FlightNr { get; set; }
        public DateTime FlightDate { get; set; }
        public bool IsShipmentFinalized { get; set; }
        public List<ParcelsBagViewModel> BagsOfParcels { get; } =
            new List<ParcelsBagViewModel>();
        public List<LettersBagViewModel> BagsOfLetters { get; } =
            new List<LettersBagViewModel>();
    }
}
