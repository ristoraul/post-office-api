﻿using System.Collections.Generic;
using Data;

namespace Facade.Shipment
{
    public class ShipmentViewModelsList : List<ShipmentViewModel>
    {
        public ShipmentViewModelsList(IEnumerable<ShipmentEntity> l)
        {
            if (l is null) return;
            foreach (var e in l)
            {
                Add(ShipmentViewModelFactory.Create(e));
            }
        }
    }
}
