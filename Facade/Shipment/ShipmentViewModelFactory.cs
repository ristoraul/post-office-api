﻿using Data;
using Facade.LettersBag;
using Facade.ParcelsBag;

namespace Facade.Shipment
{
    public class ShipmentViewModelFactory
    {
        public static ShipmentViewModel Create(ShipmentEntity o)
        {
            
            var v = new ShipmentViewModel
            {
                Id = o.Id,
                FlightDate = o.FlightDate,
                ShipmentNr = o.ShipmentNr,
                Airport = o.Airport,
                FlightNr = o.FlightNr,
                IsShipmentFinalized = o.IsShipmentFinalized
                
            };
            foreach (var c in o.Bags)
            {
                if (c.BagType == BagEntity.BagTypeEnum.Parcels)
                {
                    v.BagsOfParcels.Add(ParcelsBagViewModelFactory.Create(c));
                }
                else
                {
                    v.BagsOfLetters.Add(LettersBagViewModelFactory.Create(c));
                }

            }

            return v;
        }
    }
}
