﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Facade.Shipment;
using Microsoft.AspNetCore.Mvc;
using PostOfficeAPI.Data;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace PostOfficeAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ShipmentController : ControllerBase
    {
        private readonly ShipmentRepository _repository;

        public ShipmentController(ShipmentRepository repository)
        {
            _repository = repository;
        }

        // GET: api/<ShipmentController>
        [HttpGet]
        public ActionResult<IEnumerable<ShipmentViewModel>> GetShipments()
        {
            return _repository.GetObjectsList().ToList();
        }

        // GET api/<ShipmentController>/5
        [HttpGet("{id}")]
        public async Task<ActionResult<ShipmentViewModel>> GetShipment(int id)
        {
            var shipment = await _repository.GetObject(id);

            if (shipment == null) return NotFound();

            return shipment;
        }

        // POST api/<ShipmentController>
        [HttpPost]
        public async Task<ActionResult<ShipmentViewModel>> PostShipment(ShipmentViewModel shipment)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);
            var result = await _repository.AddObject(shipment);

            if (result == null)
                //List is necessary for displaying errors in React as they share the same data structure as fluent validation errors
                return Conflict(new
                    {errors = new List<string> {$"Shipment number '{shipment.ShipmentNr}' already exists."}});


            return CreatedAtAction("GetShipment", new {id = result.Id}, result);
        }

        // PUT api/<ShipmentController>/5
        [HttpPut("{id}")]
        public async Task<ActionResult<ShipmentViewModel>> PutShipment(int id, ShipmentViewModel shipment)
        {
            var errors = new List<string>();
            if (id != shipment.Id) return BadRequest();
            if (shipment.FlightDate < DateTime.Now)
            {
                errors.Add($"Flight date '{shipment.FlightDate}' cannot be in the past");
                return BadRequest(new { errors });
            }
            var shipmentForValidation = _repository.GetObject(shipment.Id).Result;
            if (shipmentForValidation == null) return NotFound();
            if (!shipmentForValidation.BagsOfParcels.Any() && !shipmentForValidation.BagsOfLetters.Any())
            {
                errors.Add($"Shipment number '{shipment.ShipmentNr}' does not contain any bags");
                return BadRequest(new {errors});
            }

            if (shipmentForValidation.BagsOfParcels.Select(x => x.AmountOfParcels).Any(x => x < 1))
            {
                errors.Add($"Shipment number '{shipment.ShipmentNr}' bags cannot be empty");
                return BadRequest(new {errors});
            }

            var result = await _repository.UpdateObject(shipment);
            if (result == null) return Conflict();

            return CreatedAtAction("GetShipment", new {id = result.Id}, result);
        }

        // DELETE api/<ShipmentController>/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteShipment(int id)
        {
            ShipmentViewModel shipment = await _repository.GetObject(id);
            if (shipment == null)
            {
                return NotFound();
            }

            await _repository.DeleteObject(shipment);

            return Ok();
        }

    }
}