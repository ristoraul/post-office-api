﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Facade.Parcel;
using Microsoft.AspNetCore.Mvc;
using PostOfficeAPI.Data;

namespace PostOfficeAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ParcelController : ControllerBase
    {
        private readonly ParcelsBagRepository _parcelsBagRepository;
        private readonly ParcelRepository _repository;
        private readonly ShipmentRepository _shipmentRepository;

        public ParcelController(ParcelRepository repository, ShipmentRepository shipmentRepository,
            ParcelsBagRepository parcelsBagRepository)
        {
            _repository = repository;
            _shipmentRepository = shipmentRepository;
            _parcelsBagRepository = parcelsBagRepository;
        }

        //GET: api/<ParcelController>
        [HttpGet]
        public ActionResult<IEnumerable<ParcelViewModel>> GetParcels()
        {
            return _repository.GetObjectsList().ToList();
        }

        // GET api/<ParcelController>/5
        [HttpGet("{id}")]
        public async Task<ActionResult<ParcelViewModel>> GetParcel(int id)
        {
            var parcel = await _repository.GetObject(id);
            if (parcel.Id != id) return NotFound();

            return parcel;
        }

        //POST api/<ParcelController>
        [HttpPost]
        public async Task<ActionResult<ParcelViewModel>> PostParcel(ParcelViewModel parcel)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);
            var errors = new List<string>();
            var parcelsBag = _parcelsBagRepository.GetObject(parcel.BagId).Result;
            if (parcelsBag == null) return NotFound();
            var shipment = _shipmentRepository.GetObject(parcelsBag.ShipmentId).Result;
            if (shipment == null) return NotFound();
            if (shipment.IsShipmentFinalized)
            {
                errors.Add(
                    $"Bag number '{parcelsBag.BagNr}' in shipment number '{shipment.ShipmentNr} is already finalized");
                return BadRequest(new {errors});
            }

            var result = await _repository.AddObject(parcel);
            if (result == null)
            {
                errors.Add($"Parcel number '{parcel.ParcelNr}' already exists.");
                return Conflict(new {errors});
            }

            return CreatedAtAction("GetParcel", new {id = result.Id}, result);
        }

        // PUT api/<ParcelController>/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value) //not needed right now
        {
        }

        // DELETE api/<ParcelController>/5
        [HttpDelete("{id}")]
        public void Delete(int id) //not needed right now
        {
        }
    }
}