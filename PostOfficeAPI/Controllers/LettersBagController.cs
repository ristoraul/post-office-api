﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Facade.LettersBag;
using Microsoft.AspNetCore.Mvc;
using PostOfficeAPI.Data;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace PostOfficeAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LettersBagController : ControllerBase
    {
        private readonly LettersBagRepository _repository;
        private readonly ShipmentRepository _shipmentRepository;

        public LettersBagController(LettersBagRepository repository, ShipmentRepository shipmentRepository)
        {
            _repository = repository;
            _shipmentRepository = shipmentRepository;
        }

        // GET: api/<LettersBagController>
        [HttpGet]
        public ActionResult<IEnumerable<LettersBagViewModel>> GetLettersBags()
        {
            return _repository.GetObjectsList().ToList();
        }

        // GET api/<LettersBagController>/5
        [HttpGet("{id}")]
        public async Task<ActionResult<LettersBagViewModel>> GetLettersBag(int id)
        {
            var parcel = await _repository.GetObject(id);
            if (parcel.Id != id) return NotFound();

            return parcel;
        }

        // POST api/<LettersBagController>
        [HttpPost]
        public async Task<ActionResult<LettersBagViewModel>> PostLettersBag(LettersBagViewModel lettersBag)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);
            var errors = new List<string>();
            var shipment = _shipmentRepository.GetObject(lettersBag.ShipmentId).Result;
            if (shipment == null) return NotFound();

            if (shipment.IsShipmentFinalized)
            {
                errors.Add($"Shipment number '{shipment.ShipmentNr}' is already finalized");
                return BadRequest(new {errors});
            }

            var result = await _repository.AddObject(lettersBag);
            if (result == null)
            {
                errors.Add($"Bag number '{lettersBag.BagNr}' already exists.");
                return Conflict(new {errors});
            }

            return CreatedAtAction("GetLettersBag", new {id = result.Id}, result);
        }

        // PUT api/<LettersBagController>/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value) //not needed right now
        {
        }

        // DELETE api/<LettersBagController>/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteLettersBag(int id) //not needed right now
        {
            LettersBagViewModel lettersBag = await _repository.GetObject(id);
            if (lettersBag == null)
            {
                return NotFound();
            }

            await _repository.DeleteObject(lettersBag);

            return Ok();
        }
    }
}