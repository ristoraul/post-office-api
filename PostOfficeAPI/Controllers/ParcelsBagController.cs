﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Facade.ParcelsBag;
using Microsoft.AspNetCore.Mvc;
using PostOfficeAPI.Data;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace PostOfficeAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ParcelsBagController : ControllerBase
    {
        private readonly ParcelsBagRepository _repository;
        private readonly ShipmentRepository _shipmentRepository;

        public ParcelsBagController(ParcelsBagRepository repository, ShipmentRepository shipmentRepository)
        {
            _repository = repository;
            _shipmentRepository = shipmentRepository;
        }

        // GET: api/<ParcelsBagController>
        [HttpGet]
        public ActionResult<IEnumerable<ParcelsBagViewModel>> GetParcelsBags()
        {
            return _repository.GetObjectsList().ToList();
        }

        // GET api/<ParcelsBagController>/5
        [HttpGet("{id}")]
        public async Task<ActionResult<ParcelsBagViewModel>> GetParcelsBag(int id)
        {
            var bag = await _repository.GetObject(id);
            if (bag.Id != id) return NotFound();

            return bag;
        }

        // POST api/<ParcelsBagController>
        [HttpPost]
        public async Task<ActionResult<ParcelsBagViewModel>> PostParcelsBag(ParcelsBagViewModel parcelsBag)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);
            //List is necessary for displaying errors in React as they share the same data structure as fluent validation errors
            var errors = new List<string>();
            var shipment = _shipmentRepository.GetObject(parcelsBag.ShipmentId).Result;
            if (shipment == null) return NotFound();

            if (shipment.IsShipmentFinalized)
            {
                errors.Add($"Shipment number '{shipment.ShipmentNr}' is already finalized");
                return BadRequest(new {errors});
            }

            var result = await _repository.AddObject(parcelsBag);

            if (result == null)
            {
                errors.Add($"Bag number '{parcelsBag.BagNr}' already exists.");
                return Conflict(new {errors});
            }

            return CreatedAtAction("GetParcelsBag", new {id = result.Id}, result);
        }

        // PUT api/<ParcelsBagController>/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value) //not needed right now
        {
        }

        // DELETE api/<ParcelsBagController>/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteParcelsBag(int id) //not needed right now
        {
            ParcelsBagViewModel parcelsBag = await _repository.GetObject(id);
            if (parcelsBag == null)
            {
                return NotFound();
            }

            await _repository.DeleteObject(parcelsBag);

            return Ok();
        }
    }
}