﻿using System;
using Data;
using Microsoft.EntityFrameworkCore;

namespace PostOfficeAPI
{
    public class DataContext : DbContext
    {
        public DataContext(DbContextOptions<DataContext> options) : base(options)
        {
        }

        public DbSet<ParcelEntity> ParcelsDbSet { get; set; }
        public DbSet<ShipmentEntity> ShipmentDbSet { get; set; }
        public DbSet<BagEntity> BagDbSet { get; set; }


        protected override void OnModelCreating(ModelBuilder b)
        {
            base.OnModelCreating(b);

            b.Entity<BagEntity>(entity =>
            {

                
                entity.HasOne(x => x.Shipment)
                    .WithMany(x => x.Bags)
                    .HasForeignKey(x => x.ShipmentId);
                entity.ToTable("Bags").HasAlternateKey(x => x.BagNr);
                entity.HasData(new BagEntity
                {
                    Id = 1,
                    BagType = BagEntity.BagTypeEnum.Parcels,
                    ShipmentId = 1,
                    BagNr = "asd12"
                });
                entity.HasData(new BagEntity
                {
                    Id = 2,
                    BagType = BagEntity.BagTypeEnum.Parcels,
                    ShipmentId = 3,
                    BagNr = "dasasdd2"
                });
                entity.HasData(new BagEntity
                {
                    Id = 3,
                    BagType = BagEntity.BagTypeEnum.Letters,
                    ShipmentId = 1,
                    BagNr = "123asd1",
                    CountOfLetters = 5,
                    Price = 10.5m,
                    Weight = 10
                });
            });
            b.Entity<ParcelEntity>(entity =>
            {
                entity.HasOne(x => x.Bag)
                    .WithMany(x => x.Parcels)
                    .HasForeignKey(x => x.BagId);
                entity.ToTable("Parcels").HasAlternateKey(x => x.ParcelNr);
                entity.HasData(new ParcelEntity
                {
                    Id = 1,
                    ParcelNr = "LL000000LL",
                    DestinationCountry = "EN",
                    BagId = 1,
                    Price = 5,
                    Weight = 0.5m,
                    RecipientName = "Mary"
                });
            });
            b.Entity<ShipmentEntity>(entity =>
            {
                entity.ToTable("Shipments").HasAlternateKey(x => x.ShipmentNr);
                entity.HasData(new ShipmentEntity
                {
                    Id = 1,
                    Airport = Airport.HEL,
                    FlightDate = new DateTime(2021, 12, 12),
                    FlightNr = "PP2019",
                    ShipmentNr = "PPP-PPPPPP",
                    IsShipmentFinalized = true
                });
                entity.HasData(new ShipmentEntity
                {
                    Id = 2,
                    Airport = Airport.HEL,
                    FlightDate = new DateTime(2021, 02, 06),
                    FlightNr = "PP2020",
                    ShipmentNr = "PPP-PPPPPR",
                    IsShipmentFinalized = false
                });
                entity.HasData(new ShipmentEntity
                {
                    Id = 3,
                    Airport = Airport.HEL,
                    FlightDate = new DateTime(2021, 12, 12),
                    FlightNr = "PP2020",
                    ShipmentNr = "PPP-PPPPPT",
                    IsShipmentFinalized = false
                });
            });
        }
    }
}