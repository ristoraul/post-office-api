﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Data;
using Facade.LettersBag;

namespace PostOfficeAPI.Data
{
    public class LettersBagRepository : IObjectRepository<LettersBagViewModel>
    {
        private readonly DataContext _context;

        public LettersBagRepository(DataContext context)
        {
            _context = context;
        }

        public IEnumerable<LettersBagViewModel> GetObjectsList()
        {
            var query = _context.BagDbSet.Where(x => x.BagType == BagEntity.BagTypeEnum.Letters)
                .AsEnumerable();
            return new LettersBagViewModelsList(query);
        }

        public async Task<LettersBagViewModel> GetObject(int id)
        {
            var pp = await _context.BagDbSet.FindAsync(id);
            if (pp == null) return null;
            return LettersBagViewModelFactory.Create(pp);
        }

        public async Task<LettersBagViewModel> AddObject(LettersBagViewModel lettersBag)
        {
            LettersBagViewModel result = null;

            var entity = new BagEntity
            {
                BagNr = lettersBag.BagNr,
                ShipmentId = lettersBag.ShipmentId,
                BagType = BagEntity.BagTypeEnum.Letters,
                CountOfLetters = lettersBag.CountOfLetters,
                Price = lettersBag.Price,
                Weight = lettersBag.Weight
            };
            try
            {
                await _context.BagDbSet.AddAsync(entity);

                var i = await _context.SaveChangesAsync();
                if (i >= 1) result = await GetObject(entity.Id);
            }
            catch (Exception)
            {
                result = null;
            }

            return result;
        }
        public async Task<bool> DeleteObject(LettersBagViewModel lettersBag)
        {

            var entity = new BagEntity
            {
                BagNr = lettersBag.BagNr,
                ShipmentId = lettersBag.ShipmentId,
                BagType = BagEntity.BagTypeEnum.Letters,
                CountOfLetters = lettersBag.CountOfLetters,
                Price = lettersBag.Price,
                Weight = lettersBag.Weight
            };
            bool result = false;
            try
            {
                _context.BagDbSet.Remove(entity);

                int i = await _context.SaveChangesAsync();
                if (i >= 1)
                {
                    result = true;
                }
            }
            catch (Exception)
            {
                result = false;
            }

            return result;
        }
    }
}