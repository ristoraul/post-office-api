﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Data;
using Facade.ParcelsBag;
using Microsoft.EntityFrameworkCore;

namespace PostOfficeAPI.Data
{
    public class ParcelsBagRepository : IObjectRepository<ParcelsBagViewModel>
    {
        private readonly DataContext _context;
        private readonly ShipmentRepository _shipmentRepository;

        public ParcelsBagRepository(DataContext context, ShipmentRepository shipmentRepository)
        {
            _context = context;
            _shipmentRepository = shipmentRepository;
        }

        public IEnumerable<ParcelsBagViewModel> GetObjectsList()
        {
            var query = _context.BagDbSet.Where(x => x.BagType == BagEntity.BagTypeEnum.Parcels)
                .Include(x => x.Parcels)
                .AsEnumerable();
            return new ParcelsBagViewModelsList(query);
        }

        public async Task<ParcelsBagViewModel> GetObject(int id)
        {
            var pp = await _context.BagDbSet
                .Include(x => x.Parcels)
                .FirstOrDefaultAsync(x => x.Id == id);
            if (pp == null) return null;
            return ParcelsBagViewModelFactory.Create(pp);
        }

        public async Task<ParcelsBagViewModel> AddObject(ParcelsBagViewModel parcelsBag)
        {
            ParcelsBagViewModel result = null;

            var entity = new BagEntity
            {
                BagNr = parcelsBag.BagNr,
                ShipmentId = parcelsBag.ShipmentId,
                BagType = BagEntity.BagTypeEnum.Parcels
            };
            try
            {
                await _context.BagDbSet.AddAsync(entity);

                var i = await _context.SaveChangesAsync();
                if (i >= 1) result = await GetObject(entity.Id);
            }
            catch (Exception)
            {
                result = null;
            }

            return result;
        }
        public async Task<bool> DeleteObject(ParcelsBagViewModel parcelsBag)
        {
            var entity = new BagEntity
            {
                BagNr = parcelsBag.BagNr,
                ShipmentId = parcelsBag.ShipmentId,
                BagType = BagEntity.BagTypeEnum.Parcels
            };
            bool result = false;
            try
            {
                _context.BagDbSet.Remove(entity);

                int i = await _context.SaveChangesAsync();
                if (i >= 1)
                {
                    result = true;
                }
            }
            catch (Exception)
            {
                result = false;
            }

            return result;
        }
    }
}