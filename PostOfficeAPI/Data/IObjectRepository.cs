﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Facade;

namespace PostOfficeAPI.Data
{
    public interface IObjectRepository<T> where T : ViewModelBase
    {
        Task<T> GetObject(int id);
        Task<T> AddObject(T o);
        IEnumerable<T> GetObjectsList();
        //Task<bool> DeleteObject(T o);
        //Task UpdateObject(T o);
    }
}
