﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Data;
using Facade.Shipment;
using Microsoft.EntityFrameworkCore;

namespace PostOfficeAPI.Data
{
    public class ShipmentRepository : IObjectRepository<ShipmentViewModel>
    {
        private readonly DataContext _context;

        public ShipmentRepository(DataContext context)
        {
            _context = context;
        }

        public IEnumerable<ShipmentViewModel> GetObjectsList()
        {
            var query = _context.ShipmentDbSet
                .Include(x => x.Bags)
                .ThenInclude(x => x.Parcels)
                .AsEnumerable();

            return new ShipmentViewModelsList(query);
        }

        public async Task<ShipmentViewModel> GetObject(int id)
        {
            var pp = await _context.ShipmentDbSet
                .Include(x => x.Bags)
                .ThenInclude(x => x.Parcels).AsNoTracking()
                .FirstOrDefaultAsync(x => x.Id == id);
            if (pp == null) return null;

            return ShipmentViewModelFactory.Create(pp);
        }

        public async Task<ShipmentViewModel> AddObject(ShipmentViewModel shipment)
        {
            ShipmentViewModel result = null;
            var entity = new ShipmentEntity
            {
                Airport = shipment.Airport,
                FlightDate = shipment.FlightDate,
                FlightNr = shipment.FlightNr,
                ShipmentNr = shipment.ShipmentNr,
                IsShipmentFinalized = shipment.IsShipmentFinalized
            };
            try
            {
                await _context.ShipmentDbSet.AddAsync(entity);

                var i = await _context.SaveChangesAsync();
                if (i >= 1) result = await GetObject(entity.Id);
            }
            catch (Exception)
            {
                result = null;
            }

            return result;
        }

        public async Task<ShipmentViewModel> UpdateObject(ShipmentViewModel shipment)
        {
            var pp = new ShipmentEntity
            {
                Id = shipment.Id,
                FlightDate = shipment.FlightDate,
                FlightNr = shipment.FlightNr,
                Airport = shipment.Airport,
                IsShipmentFinalized = shipment.IsShipmentFinalized,
                ShipmentNr = shipment.ShipmentNr
            };
            ShipmentViewModel result = null;
            try
            {
                _context.Entry(pp).State = EntityState.Modified;
                var i = await _context.SaveChangesAsync();
                if (i >= 1) result = await GetObject(pp.Id);
            }
            catch (Exception e)
            {
                var s = e.Message;
                result = null;
            }

            return result;
        }
        public async Task<bool> DeleteObject(ShipmentViewModel shipment)
        {
            var pp = new ShipmentEntity
            {
                Id = shipment.Id,
                FlightDate = shipment.FlightDate,
                FlightNr = shipment.FlightNr,
                Airport = shipment.Airport,
                IsShipmentFinalized = shipment.IsShipmentFinalized,
                ShipmentNr = shipment.ShipmentNr
            };
            bool result = false;
            try
            {
                _context.ShipmentDbSet.Remove(pp);

                int i = await _context.SaveChangesAsync();
                if (i >= 1)
                {
                    result = true;
                }
            }
            catch (Exception)
            {
                result = false;
            }

            return result;
        }

    }
}