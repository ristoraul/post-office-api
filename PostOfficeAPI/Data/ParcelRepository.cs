﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Data;
using Facade.Parcel;

namespace PostOfficeAPI.Data
{
    public class ParcelRepository : IObjectRepository<ParcelViewModel>
    {
        private readonly DataContext _context;

        public ParcelRepository(DataContext context)
        {
            _context = context;
        }

        public IEnumerable<ParcelViewModel> GetObjectsList()
        {
            var query = _context.ParcelsDbSet.AsEnumerable();
            return new ParcelViewModelsList(query);
        }

        public async Task<ParcelViewModel> GetObject(int id)
        {
            var pp = await _context.ParcelsDbSet.FindAsync(id);
            if (pp == null) return null;
            return ParcelViewModelFactory.Create(pp);
        }

        public async Task<ParcelViewModel> AddObject(ParcelViewModel parcel)
        {
            //can't insert parcels into letters bag
            if (_context.BagDbSet.FindAsync(parcel.BagId).Result.BagType == BagEntity.BagTypeEnum.Letters
            ) 
                return null;
            var entity = new ParcelEntity
            {
                DestinationCountry = parcel.DestinationCountry,
                ParcelNr = parcel.ParcelNr,
                BagId = parcel.BagId,
                Price = parcel.Price,
                Weight = parcel.Weight,
                RecipientName = parcel.RecipientName
            };
            ParcelViewModel result = null;
            try
            {
                await _context.ParcelsDbSet.AddAsync(entity);

                var i = await _context.SaveChangesAsync();
                if (i >= 1) result = await GetObject(entity.Id);
            }
            catch (Exception)
            {
                result = null;
            }

            return result;
        }
    }
}