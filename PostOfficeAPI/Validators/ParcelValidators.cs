﻿using Facade.Parcel;
using FluentValidation;

namespace PostOfficeAPI.Validators
{
    public class ParcelValidator : AbstractValidator<ParcelViewModel>
        {
            public ParcelValidator()
            {

                RuleFor(x => x.ParcelNr)
                    .NotEmpty()
                    .NotNull()
                    .Matches("^[a-zA-Z]{2}[0-9]{6}[a-zA-Z]{2}$").OverridePropertyName("Parcel number");
                RuleFor(x => x.RecipientName)
                    .NotEmpty()
                    .NotNull()
                    .MaximumLength(100);
                RuleFor(x => x.DestinationCountry)
                    .NotEmpty()
                    .NotNull()
                    .Matches("^[A-Z]{2}$").OverridePropertyName("Destination country");
                RuleFor(x => x.Weight)
                    .GreaterThanOrEqualTo(0);
                RuleFor(x => x.Price)
                    .GreaterThanOrEqualTo(0);


                // ^[1-9]\d{0,10}(?:,\d{1,3})?$

            }
        }
}
