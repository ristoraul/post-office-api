﻿using Facade.Shipment;
using FluentValidation;

namespace PostOfficeAPI.Validators
{
    public class ShipmentValidators: AbstractValidator<ShipmentViewModel>
    {
        public ShipmentValidators()
        {
            RuleFor(x => x.ShipmentNr)
                .NotEmpty()
                .NotNull()
                .Matches("^[a-zA-Z0-9]{3}-[a-zA-Z0-9]{6}$").OverridePropertyName("Shipment number");
            RuleFor(x => x.FlightNr)
                .NotEmpty()
                .NotNull()
                .Matches("^[a-zA-Z]{2}[0-9]{4}$").OverridePropertyName("Flight number");
            RuleFor(x => x.Airport.ToString())
                .NotEmpty()
                .NotNull();
            RuleFor(x => x.FlightDate)
                .NotEmpty()
                .NotNull().OverridePropertyName("Flight date");


        }
    }
}
