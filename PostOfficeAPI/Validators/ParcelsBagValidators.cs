﻿using Facade.ParcelsBag;
using FluentValidation;

namespace PostOfficeAPI.Validators
{
    public class ParcelsBagValidators :  AbstractValidator<ParcelsBagViewModel>
    {
        public ParcelsBagValidators()
        {
            RuleFor(x => x.BagNr)
                .NotEmpty()
                .NotNull()
                .Matches("^[a-zA-Z0-9]*$")
                .MaximumLength(15).OverridePropertyName("Bag number");
        }
    }
}
