﻿using Facade.LettersBag;
using FluentValidation;

namespace PostOfficeAPI.Validators
{
    public class LettersBagValidators : AbstractValidator<LettersBagViewModel>
    {
        public LettersBagValidators()
        {
            RuleFor(x => x.BagNr)
                .NotEmpty()
                .NotNull()
                .Matches("^[a-zA-Z0-9]*$")
                .MaximumLength(15).OverridePropertyName("Bag number");
            RuleFor(x => x.CountOfLetters)
                .GreaterThan(0)
                .OverridePropertyName("Count of letters");
            RuleFor(x => x.Price)
                .GreaterThanOrEqualTo(0);
            RuleFor(x => x.Weight)
                .GreaterThanOrEqualTo(0);
        }
    }
}
