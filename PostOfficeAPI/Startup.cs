using Facade.LettersBag;
using Facade.ParcelsBag;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Facade.Parcel;
using Facade.Shipment;
using FluentValidation;
using FluentValidation.AspNetCore;
using Microsoft.EntityFrameworkCore;
using PostOfficeAPI.Data;
using PostOfficeAPI.Validators;

namespace PostOfficeAPI
{
    public class Startup
    {
        public readonly string allowedOrigins = "http://localhost:3000";
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<DataContext>(options =>
                options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection")));
            //services.AddDbContext<DataContext>(options =>
            //    options.UseInMemoryDatabase("MyDatabase"));
            services.AddScoped<ParcelRepository>();
            services.AddScoped<ParcelsBagRepository>();
            services.AddScoped<ShipmentRepository>();
            services.AddScoped<LettersBagRepository>();
            services.AddControllers().AddFluentValidation();
            services.AddTransient<IValidator<ParcelViewModel>, ParcelValidator>();
            services.AddTransient<IValidator<ShipmentViewModel>, ShipmentValidators>();
            services.AddTransient<IValidator<ParcelsBagViewModel>, ParcelsBagValidators>();
            services.AddTransient<IValidator<LettersBagViewModel>, LettersBagValidators>();
            services.AddCors(o => o.AddPolicy(allowedOrigins,
                builder =>
                {
                    builder
                        .WithOrigins(allowedOrigins)
                        .AllowAnyMethod()
                        .AllowAnyHeader();
                }));
            //    .AddFluentValidation(s =>
            //{
            //    s.RegisterValidatorsFromAssemblyContaining<Startup>();
            //    s.RunDefaultMvcValidationAfterFluentValidationExecutes = false;
            //});
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();

            app.UseRouting();
            app.UseCors(allowedOrigins);

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
