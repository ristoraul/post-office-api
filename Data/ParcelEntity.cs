﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Data
{
    public class ParcelEntity : UniqueEntityData
    {
        [Required]
        public int BagId { get; set; }
        [Required]
        public string ParcelNr { get; set; }
        [Required]
        [Column(TypeName = "VARCHAR(100)")]
        public string RecipientName { get; set; }
        [Required]
        public string DestinationCountry { get; set; }
        [Required]
        public decimal Weight { get; set; }
        [Required]
        public decimal Price { get; set; }
        public BagEntity Bag { get; set; }


    }
}
