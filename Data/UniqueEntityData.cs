﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Data
{
    public class UniqueEntityData
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
    }
}
