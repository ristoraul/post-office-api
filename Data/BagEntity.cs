﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Data
{
    public class BagEntity : UniqueEntityData
    {
        [Required]
        public string BagNr { get; set; }
        [Required]
        public int ShipmentId { get; set; }
        [Required]
        public BagTypeEnum BagType { get; set; }
        public int CountOfLetters { get; set; }
        public decimal Weight { get; set; }
        public decimal Price { get; set; }
        public ShipmentEntity Shipment { get; set; }
        public ICollection<ParcelEntity> Parcels { get; set; }

        public enum BagTypeEnum
        {
            Letters,
            Parcels
        }
    }
}
