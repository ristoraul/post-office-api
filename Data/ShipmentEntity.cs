﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Data
{
    public class ShipmentEntity : UniqueEntityData
    {
        [Required]
        public string ShipmentNr { get; set; }
        [Required]
        public string FlightNr { get; set; }
        [Required]
        public Airport Airport { get; set; }
        [Required]
        public DateTime FlightDate { get; set; }
        public ICollection<BagEntity> Bags { get; set; }
        [Required]
        public bool IsShipmentFinalized { get; set; } = false;

    }

    public enum Airport
    {
        TLL = 1,
        RIX = 2,
        HEL = 3
    }
}
